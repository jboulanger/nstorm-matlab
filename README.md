# Analysis of single molecule localization microscopy data

This matlab toolbox enable a streamlined analysis protocole for single molecule localization microscopy data. In particular it facilitate the loading of list.txt files, the estimation of the localization precision, the computation of Fourier shell correlation and the analysis of clusters.

The file demo1.m is the place where to start

## Content:
### Input/Output
- nstormread makes it easier to load a NSTORM file
### Visualization
- nstormshow display the localization as a point cloud in 2D or in 3D
- nstormgrid create a grid for rendering
- nstormrender render the image in 3D
- nstormimshow display the rendered image as 3 maximum intensity projections (XY,XZ,YZ).
### Cropping and selection
- nstormsubset is useful to filter localization by precision or number of photons
- nstormcrop: crop interactively a region of interest
### Resolution analysis
In order to infer the resolution in x,y and z, the z calibration is retreived from the data as well as the photon conversion gain.
- nstormzcalib: retreives the parameter Wxy which is used for estimating the link between the aspect of the molecule and its z position.
- nstormmcaccuracy: estimate the localization n x,y, and z using monte-carlo simulation.
- nstormfsc: split the dataset in 2 and render it (nstormrender) using the localization accuracy to compute the Fourier shell correlation in order to get an estimate of the image resolution
- nstormfscmap : compute a Fourier Shell Correlation map
### Cluster analysis
- nstormdbscan : perform a dbscan analysis of the point cloud
- nstormpcf : compute the pair correlation function
- nstorm
