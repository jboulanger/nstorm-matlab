%% Pick and load a file
addpath('../src')
clear all;close all;
tic;
molecules = nstormread();
pixel_size = 160;
toc;
%% Correct photon per pixels 
% Localization accuracy depends on the gain calibration. One can update the
% gain calibration to recompute the Lateral Localization Accuracy stored in
% the file. (original value:0.0631)
molecules = nstormupdateadugain(molecules, 0.0631, pixel_size);
nstormthomsonlocalizationaccuracy(molecules, pixel_size);
%% Clean up
% We filter out localization with less than 1000 photons
crop = nstormsubset(molecules, molecules.photons > 1000);
subplot(221), histogram(log10(crop.photons));
title('Photons');
xlabel('Log_{10} Photons')
ylabel('Counts')
axis square
subplot(222), histogram(crop.localization_accuracy);
title('Localization accuracy');
xlabel('Accuracy [nm]')
ylabel('Counts')
axis square
subplot(223), histogram(crop.localization_accuracy);
nstormshow(crop,'xy')

%% Z Calibration extraction
crop = nstormzcalib(crop);

%% Extract a region of interest
% We focus the subsequent analysis on a small area
clf
[crop,selection] = nstormcrop(crop);
%crop = nstormcrop(molecules,selection);

%% Estimate localization precision
% We use now the z calibration to estimate the accuracy by simulation
n = 7; % number of pixels used for fitting
pixel_size = 160;% camera pixel size in nm
M = 5; % number of samples
crop = nstormmcaccuracy(crop, n, pixel_size, M);

%% Render the molecules as an image
% Here we test the rendering of the image in 3D which is later used for
% computing the Fourier shell correlation.
tic;
[x,y,z,cx,cy,cz] = nstormgrid(crop,[10,10,10]);
im = nstormrender(crop,x,y,z,'sigma0');
nstormimshow(im,cx,cy,cz);
toc

%% Resolution estimation by Fourier shell correlation
% Finally, we estimate the image resolution by computing the Fourier shell
% correlation which mixes information on localization precision and
% molecule density.
tic;
clf;
nstormfsc(crop, [10 10 10]);
toc;

%% Compute a resolution map using Fourier shell correlation
tic;
nstormfscmap(crop, [10 10 10], [500,500,500]);
toc;
%% Pair correlation function
clf;
[g,r] = nstormpcf(crop, 1000);

%% 3D Voronoi tesselation cluster analysis
[V,K,CC] = nstormvoronoi(crop, 50, 100^3);

%% Clustering by nearest neighbor analysis
nstormclusterdist(crop, 10, 100, 4, 500);

%% Clustering by nearest neighbor analysis
nstormdbscan(crop, 100, 5);

