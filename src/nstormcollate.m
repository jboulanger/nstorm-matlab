function dst = nstormcollate(src)
dst = src(1);
fn = fieldnames(dst);
for k = 1:numel(fn)
    if strcmp(fn{k},'channel_names') || strcmp(fn{k},'number') || strcmp(fn{k},'photon_per_grayvalue') || strcmp(fn{k},'zcalib')
        dst = setfield(dst,fn{k}, getfield(src(l),fn{k}));
    else
        for l = 2:numel(src)
            v1 = getfield(dst, fn{k});
            v2 = getfield(src(l), fn{k});
            dst = setfield(dst, fn{k}, [v1; v2]);
        end
    end
end
dst.number = numel(dst.x);