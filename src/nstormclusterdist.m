function labels = nstormclusterdist(molecules, nd, dmin, ndims, threshold)
% nstormclusterdist(molecules, nd, dmin, ndims, threshold)
%
% nd: number of nearest neighbord
% dmin : max distance between points in a cluster
% ndims : dimension reduction
% threshold : thresholding
%
% Shitty idea of clustering histogram of distance or nearest neighbors
% computed for each point as a descriptor.
% 
%
% Jerome Boulanger

X = [molecules.x, molecules.y, molecules.z];
n = size(X,1);
D0 = pdist2(X,X); % pairwise distance
% compute histogram of distances (~pair correlation)
v = 0:10:500; % edges of the histogram
D = zeros(numel(v)-1,n);
for k=1:n
    D(:,k) = histcounts(D0(:,k),v);
end

%imagesc(D);pause;
%% dimension reduction
[U,~,~] = svd(D);
C = U(:,1:ndims)'*D;
%imagesc(C); pause;

%% DBSCAN on the C space
labels = dbscan(C',20,ndims+1);
%scatter3(C(1,:),C(2,:),C(3,:),30,labels,'.');pause;
scatter3(X(:,1)/1000,X(:,2)/1000,X(:,3)/1000,100,labels,'.')
title('Clustering')
xlabel('X [um]')
ylabel('Y [um]')
zlabel('Z [um]')