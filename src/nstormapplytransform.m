function molecules = nstormapplytransform(molecules,trans)

cx2 = mean(molecules.x);
cy2 = mean(molecules.y);
x2 = molecules.x - trans.cx2;
y2 = molecules.y - trans.cy2;
X = [ones(molecules.number,1), x2, y2, x2.^2, x2.*y2, y2.^2];
MX = (trans.M*X')';

molecules.x = MX(:,1) + trans.cx1;
molecules.y = MX(:,2) + trans.cy1;
