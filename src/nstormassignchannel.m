function dst = nstormassignchannel(molecules,mode,threshold,dmax)
idx1 = find(molecules.channel==1);
x1 = molecules.x(idx1);
y1 = molecules.y(idx1);
t1 = molecules.frame(idx1);
X1 = [x1,y1];

idx2 = find(molecules.channel==2);
x2 = molecules.x(idx2);
y2 = molecules.y(idx2);
t2 = molecules.frame(idx2);
X2 = [x2, y2];

[d,p] = match(X1,X2,t1,t2);
%[d,p] = min(pdist2(X1, X2),[],2);
I = d < dmax;
r = log(molecules.height(idx1(I)));
g = log(molecules.height(idx2(p(I))));


if mode==1
    C = (r > threshold) + 2* (g>threshold);
    scatter(r,g,1,C,'.')
    box on
    axis equal
    axis square
    xlabel('top')
    ylabel('bottom')
    title('Intensity')
    grid on
else
    a = 0.5*(r+g);
    m = r-g;
    m = (m - mean(m)) ./ std(m);
    C = (m < -threshold) + 2 * (m > threshold);
    scatter(a,m,1,C,'.')
    box on
    axis equal
    axis square
    xlabel('top')
    ylabel('bottom')
    title('MvA plot')
    grid on
end

if 0
    dst = nstormsubset(molecules, idx2(p(I)));
    dst.channel = C;
else
    dst(1) = nstormsubset(molecules, idx1(I));
    dst(1).channel = C;
    dst(2) = nstormsubset(molecules, idx2(p(I)));
    dst(2).channel = C;
    dst = nstormcollate(dst);
end
dst.channel_names = {'ch1','ch2'};
unique(dst.channel)
dst = nstormsubset(dst, dst.channel==1 | dst.channel==2 );

