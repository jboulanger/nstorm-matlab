function [g,r] = nstormpcf(molecules, rmax)
% [g,r] = nstormpcf(molecules, rmax)
%
% Pair correlation function
%
% Count the average number of point around a shell centered in each point
% and normalize it by the expected number of point in the shell of radius r
% if the point process was random (density x 4 x PI x r^2 x dr).
%
% The code does not correct for edge effects at the moment
%
% Input:
%  molecules: structure with array x,y,z
%  rmax     : maximum radius
% 
% Output:
%  g        : pair correlation function
%  r        : radius vector
%
% Jerome Boulanger

X = [molecules.x, molecules.y, molecules.z];
shp = alphaShape(X); % compute the volume
v = shp.volume;
n = size(X,1);
rho = n / v; % density
dr = rmax/50;
edges = dr:dr:rmax+dr;
r = dr:dr:rmax;
D = pdist(X);
g = histcounts(D,edges);
g = g ./ (rho*4*pi*r.^2*dr*(n-1)/2);

plot(r,g,r,ones(size(r)));
title('Pair correlation')
xlabel('Radius [nm]')
ylabel('g(r)')
axis tight
axis([0, rmax, 0, max(g)]);
