function corrected = nstormdrift(molecules,K,maxiter,maxerror)
% corrected = nstormdrift(molecules,K)
%
% Correct drift by points cloud alignment
%
% input:
%  molecules: original molecules structures with fields (x,y,frame)
%  K        : number of blocks of frames
%


nframes = max(molecules.frame);
D = nstormsplit(molecules,ceil(molecules.frame/nframes*K));
%D(1) = nstormsubset(molecules, molecules.frame<nframes/K);
for k=2:K
    %idx = find(molecules.frame>(k-1)*nframes/K & molecules.frame<=k*nframes/K);
   % D(k) = nstormsubset(molecules, idx);
    trans = nstormalignchannel(D(1), D(k), maxiter, maxerror);
    D(k) = nstormapplytransform(D(k),trans);
end
corrected = nstormcollate(D);
