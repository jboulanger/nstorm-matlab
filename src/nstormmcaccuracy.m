function molecules = nstormmcaccuracy(molecules, n, pixel_size, M)
% molecules = nstormmcaccuracy(molecules, n, pixel_size, M)
%
% Jerome Boulanger 2020
tic
opts = optimset('Display','off');
sigmax = zeros(molecules.number,1);
sigmay = zeros(molecules.number,1);
sigmay = zeros(molecules.number,1);
parfor k=1:molecules.number
     [g,p0,model,xy] = nstormrender1(molecules,k,n,pixel_size);
     X = zeros(M,7);% all parameters + z (bg,height,x0,y0,wx,wy,z)
     cid = molecules.channel(k);
     for m = 1:M
         I = poissrnd(g*molecules.photon_per_grayvalue)/molecules.photon_per_grayvalue;
         X(m,1:6) = lsqcurvefit(model,p0,xy,I,[],[],opts)';
         X(m,7) = computez(X(m,5),X(m,6),molecules.zcalib(cid).wx,molecules.zcalib(cid).wy,molecules.zcalib(cid).z0);
     end
     S = std(X);
     sigmax(k) = S(3);
     sigmay(k) = S(4);
     sigmaz(k) = S(7);     
end
sigmaz(sigmaz==0) = median(sigmaz);
molecules.sigmax = sigmax(:);
molecules.sigmay = sigmay(:);
molecules.sigmaz = sigmaz(:);
toc;
C = numel(molecules.channel_names);
for c=1:C
    
    idx=molecules.channel==c;
    subplot(C,3,3*(c-1)+1);
    histogram(sigmax(idx));
    title(sprintf('X accuracy\nmed: %.2fnm',median(sigmax(idx))));
    xlabel('\sigma_x [nm]')
    ylabel('counts')
    subplot(C,3,3*(c-1)+2);
    histogram(sigmay(idx));
    title(sprintf('Y accuracy\nmed:%.2fnm',median(sigmay(idx))));
    xlabel('\sigma_y [nm]')
    ylabel('counts')
    subplot(C,3,3*(c-1)+3);
    histogram(sigmaz(idx));
    title(sprintf('Z accuracy\nmed:%.2fnm',median(sigmaz(idx))));
    xlabel('\sigma_z [nm]')
    ylabel('counts')
% subplot(324);
% plot3(sigmax,sigmay,sigmaz,'.');
% xlabel('\sigma_x [nm]')
% ylabel('\sigma_y [nm]')
% zlabel('\sigma_y [nm]')
% subplot(325);
% c = polyfit(molecules.localization_accuracy,sqrt(sigmax.^2+sigmay.^2),1);
% x0 = linspace(min(molecules.localization_accuracy),max(molecules.localization_accuracy));
% y0 = c(1).*x0 + c(2);
% fprintf('sigma = %f Localization accuracy + %f\n', c);
% plot(molecules.localization_accuracy,sqrt(sigmax.^2+sigmay.^2),'.',x0,y0)
% xlabel('Localization accuracy');
% ylabel('(\sigma_x^2+\sigma_y^2)^{1/2}');
end

function z = computez(wx,wy,wx0,wy0,z0)
[~,k] = min( (sqrt(wx)-sqrt(wx0)).^2 + (sqrt(wy)-sqrt(wy0)).^2 );
z = z0(k);
