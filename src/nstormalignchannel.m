function  trans = nstormalignchannel(mol1,mol2,maxiter,maxerror)
if nargin < 3
    maxiter = 200;
end

if nargin < 4
    maxerror = 0.5;
end

cx1 = mean(mol1.x);
cy1 = mean(mol1.y);
x1 = mol1.x - cx1;
y1 = mol1.y - cy1;
t1 = mol1.frame;

cx2 = mean(mol2.x);
cy2 = mean(mol2.y);
x2 = mol2.x - cx2;
y2 = mol2.y - cy2;
t2 = mol2.frame;

Y = [x1,y1];
X = [ones(numel(x2),1), x2, y2, x2.^2, x2.*y2, y2.^2];
M = [0,1,0,0,0,0;0,0,1,0,0,0];

MX = (M*X')';

for iter = 1:maxiter
    [d,p] = min(pdist2(Y, MX),[],2);
    %[d,p] = match(Y,MX,t1,t2);
    n = numel(p);
    I = ceil(n * rand(round(0.8*n), 1));
    %I = I(d(I) < mean(d));
    M = (pinv(X(p(I),:)) * Y(I,:))';
    %M = (pinv(X(p,:)) * Y)';
    allM(:,:,iter) = M;
    e(iter) = mean(d);
    MX = (M*X')';
    e(iter) = mean(d);

    subplot(121); 
    plot(Y(:,1),Y(:,2),'.',MX(:,1),MX(:,2),'.');
    axis tight
    axis equal

    subplot(122);
    plot(e);xlabel('iteration');ylabel('error');
    title(sprintf('%d/%d %.2f',iter,maxiter,e(iter)))
    axis square
    drawnow;
    
    if iter >10 && mean(e(max(1,iter-10):iter)) < maxerror
        break
    end
end

trans.M = mean(allM(:,:,end-10:end),3);
trans.cx1 = cx1;
trans.cy1 = cy1;
trans.cx2 = cx2;
trans.cy2 = cy2;



