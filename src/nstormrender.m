function im = nstormrender(molecules, x, y, z, method)
% im = nstormrender(molecules, x, y, z)
% 
% Render a 3D image on the grid defined by x,y,z (meshgrid) tacking into
% account the localization precision.
%
% Input:
%  molecules: structure with position in fields x,y,z and localization
%  precision in sigmax, sigmay, sigmaz
%  x,y,z : results of meshgrid on the domain
%
% Jerome Boulanger 2020

if nargin < 5
    method = 'sigma';
end

fprintf('nstorm render image of size %d x %d x %d with %d molecules\n', size(x), molecules.number);
im = zeros([size(x), numel(molecules.channel_names)]);
for c = 1:numel(molecules.channel_names)
    idx = find(molecules.channel == c);
    if ~isempty(idx)
        im(:,:,:,c) = renderchannel(molecules, idx, x, y, z, method);
    else 
        fprintf('Channel %d is empty\n', c);
    end
end


function im = renderchannel(molecules, idx, x, y, z, method)
x0 = molecules.x(idx);
y0 = molecules.y(idx);
z0 = molecules.z(idx);
if strcmp(method,'sigma')
    sx = max(1, molecules.sigmax(idx));
    sy = max(1, molecules.sigmay(idx));
    sz = max(1, molecules.sigmaz(idx));
elseif strcmp(method,'accuracy')
    sx = max(1, molecules.localization_accuracy);
    sy = max(1, molecules.localization_accuracy);
    sz = max(1, molecules.localization_accuracy);
elseif isfield(molecules,'localization_accuracy')
    s0 = median(molecules.localization_accuracy) * ones(size(x));
    sx = s0;
    sy = s0;
    sz = s0;
else
    sx = 3*(x(1,2,1)-x(1,1,1)) * ones(size(molecules.x));
    sy = sx;
    sz = sx;
end
threshold = 12;
nchunk = 12;
K = numel(x0);
for chunk = 1:nchunk
    acc(chunk).im = zeros(size(x));
end
vx = x(1,:,1);
vy = y(:,1,1);
vz = z(1,1,:);
a = 1 ./ ( (2*pi)^(3/2).* sx .* sy .* sz);
for chunk = 1:nchunk
    k0 = min(K, round(1+(chunk-1)/(nchunk)*K));
    k1 = min(K, round(chunk/(nchunk)*K));
    for k = k0:k1
        ix = ((vx - x0(k)) / sx(k)).^2 < threshold;
        iy = ((vy - y0(k)) / sy(k)).^2 < threshold;
        iz = ((vz - z0(k)) / sz(k)).^2 < threshold;
        rx = ((x(iy,ix,iz) - x0(k)) / sx(k)).^2;
        ry = ((y(iy,ix,iz) - y0(k)) / sy(k)).^2;
        rz = ((z(iy,ix,iz) - z0(k)) / sz(k)).^2;
        d = rx+ry+rz;
        acc(chunk).im(iy,ix,iz) = acc(chunk).im(iy,ix,iz) + a(k) .* exp(-0.5*d);
    end
end
im = zeros(size(x));
for chunk = 1:nchunk
    im = im + acc(chunk).im;
end


