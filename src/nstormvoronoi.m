function [V,K,CC] = nstormvoronoi(molecules, noise_percentage, threshold)
%
% [V,K,R] = voronoiSegmentation(points, noise_percentage, threshold)
%
% Segment clusters from a set set of coordinates
%
% Create a voronoi partition of the volume and aggregate neighboring cells
% 
% 
% Return the vertices V and the convexhull information K for each vertices
% and the regions R with their cells and the total volume
%
% To plot a cell use:   trisurf(K(id).hull, V(:,1), V(:,2), V(:,3))
%
% Jerome Boulanger


points = [molecules.x, molecules.y molecules.z];

% bounding box
bb = [min(points); max(points)];

N = size(points, 1);

% add random points
Nrnd = ceil(noise_percentage / 100 * N);
rnd = repmat(bb(1,:),[Nrnd 1]) + repmat(bb(2,:)-bb(1,:),[Nrnd 1]) .* rand(Nrnd,3);
pts = [points; rnd];

% create a voronoi diagram
[V,C] = voronoin(pts);

% compute connected components of voronoi cells
[K,CC] = computeconvexhullofvoronoicells(V,C,threshold);

% display the result
displayclusters(pts, V, CC, K)

end

function [K,CC] = computeconvexhullofvoronoicells(V,C,threshold)
% compute the convex hull and the volume S for each closed cell
% 
% V vertices NxD array
% C cells list of vertives making a voronoi cell
% 
% output:
% K  : graph with the cells with indices corresponds to cell indices
% CC : connex components 
%

% VOLUME ESTIMATION 
% for each voronoi cell
for id = 1:numel(C)
    if all(C{id}~=1)
        try
            VCid = V(C{id},:);
            % compute the convex hull of voronoi cell as triangles
            [Ktmp, Stmp] = convhulln(VCid);            
            % convert the indices within the cell to global indices
            K(id).hull = C{id}(Ktmp);
            K(id).volume = Stmp;
            K(id).neighbors = [];            
        catch e
            K(id).hull = [];
            K(id).volume = Inf;
            K(id).neighbors = [];            
        end
    else
        K(id).hull = [];
        K(id).volume = Inf;
        K(id).neighbors = [];        
    end
end

% GRAPH
% find convex hulls sharing a common face (triangle) and create a graph (adjaceny list)
for i = 1:numel(K)
    if ~isempty(K(i).hull) && ~isinf(K(i).volume)
        for j = i:numel(K)
            if ~isempty(K(j).hull) && ~isinf(K(j).volume)
                nlist = intersect(K(i).hull,K(j).hull); % share a triangle                
                ci = K(i).volume > threshold;
                cj = K(j).volume > threshold;
                if numel(nlist) >= 1 && ci == cj
                    K(i).neighbors = unique([K(i).neighbors; j]);
                    K(j).neighbors = unique([K(j).neighbors; i]);
                end
            end
        end
    end
end

% LABELLING
% compute the connected components of the graph adding a label for each
% vertex
labels = connectedcomponents(K);

% compute the volume of each connected component
ncolor = max(labels);
fprintf('N labels : %d\n', ncolor );
k = 1;
for c = 1:ncolor    
    idx = find(labels == c); % find the cells for this region
    %vols = [K(idx).volume]; % get the corresponding volumes
    %idx = idx(~isinf(vols)); % find the cells whose volume are not Inf
    vol = sum([K(idx).volume]);
    if ~isinf(vol) && numel(idx) > 10
        CC(k).cells = idx; % exclude the Inf 
        CC(k).volume = vol; % compute the resulting volume        
        k = k + 1;
    end
end
fprintf('N regions : %d\n', numel(CC) );
end

function displayclusters(points, V, CC, K)
alpha = 0.5;
bb = [min(points); max(points)];
cmap = lines(numel(CC));
plot3(points(:,1),points(:,2),points(:,3),'.','color',[0.8 0.8 0.8 0.5]);
hold on;
showVoronoiSegmentation(points, V, K, CC)
hold off
axis(bb(:));
end

function voronoineighborshow(points, V, K, query)
facealphalut = [0.1 0.2];
bb = [min(points); max(points)];
% display all points
plot3(points(:,1),points(:,2),points(:,3),'.','color',[0.8 0.8 0.8 0.5]);
hold on;
trisurf(K(query).hull, V(:,1), V(:,2), V(:,3),...
            'FaceAlpha', 0.5, 'EdgeAlpha',0.1,'FaceColor',[0.7 0.7 0.7]);    
plot3(points(query,1),points(query,2),points(query,3),'r.','MarkerSize',30);
for n = 1:numel(K(query).neighbors)
    id = K(query).neighbors(n);
    if K(id).volume > 0          
        alpha = facealphalut(1+ (K(id).label > 0));
        trisurf(K(id).hull, V(:,1), V(:,2), V(:,3),...
            'FaceAlpha', alpha, 'EdgeAlpha',0.1,'FaceColor',[0.0 0.2 0.6]);    
        plot3(points(id,1),points(id,2),points(id,3),'g.','MarkerSize',30);
    end
end
hold off
axis(bb(:));
grid on;
xlabel('X [nm]');
ylabel('Y [nm]');
zlabel('Z [nm]');
end
