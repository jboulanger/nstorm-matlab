function [crop,selection] = nstormcrop(storm, selection)
% [crop, selection] = nstormcrop(storm)
% crop = nstormcrop(storm, selection)
%
% Crop a set of coordinates
%
% jerome boulanger 2017 -2018

if nargin < 2
    nstormshow(storm, 'XY');
    title(sprintf('XY freehand selection \n 2 click to finish'));
    h = imfreehand;
    selection = wait(h);
    if isempty(selection)
        title('Failed: empty selection');
        crop = storm;
    else
        title('Cropping');
        in1 = inpolygon(storm.x,storm.y,selection(:,1),selection(:,2));
        crop = nstormsubset(storm, in1);
        nstormshow(crop, 'XY');
        fprintf('Selected %d/%d molecules\n', crop.number, storm.number);
    end
else
    in1 = inpolygon(storm.x,storm.y,selection(:,1),selection(:,2));
    crop = nstormsubset(storm, in1);
    fprintf('Selected %d/%d molecules\n', crop.number, storm.number);
end


