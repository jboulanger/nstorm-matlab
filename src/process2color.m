clear all
%%
filename = '/home/jeromeb/work/userdata/Light_Microscopy/nbarry/STORM for jerome/Both ratio splitter4.tif';
nframes = numel(imfinfo(filename));
f = fspecial('gaussian',[11,11],2) - fspecial('gaussian',[11,11],5);
pfa = 0.1;
st = strel('disk',2);
pixel_size_nm = 160;
tic;
parfor k = 1:nframes
    frame = imread(filename, k);
    frame = frame(:,128:end-127);
    tmp(k) = nstrormdetectmolecules(frame,f,pfa,st,k,pixel_size_nm);
end
save('tmp.mat','tmp')
toc
%% collate results
clear all;
load('tmp.mat')
pixel_size_nm = 160;
molecules = nstormcollate(tmp);
molecules = nstormsubset(molecules, find(molecules.sigmax<50 & molecules.x>500));
%molecules = nstormsubset(molecules, molecules.height > 200);
molecules.channel_names = {'a','b'};
molecules.channel = ones(size(molecules.x));
molecules.z = zeros(size(molecules.x));
molecules.sigmaz = ones(size(molecules.x));
clf, nstormshow(molecules,'xy');axis ij

%% aligned channels
subset = nstormsubset(molecules, molecules.frame < 200);
tmp = nstormsplit(subset, subset.y<256*pixel_size_nm);
trans = nstormalignchannel(tmp(1), tmp(2), 50, 50);
%%
split = nstormsplit(molecules,1+(molecules.y < 256*pixel_size_nm));
split(1).channel = ones(split(1).number,1);
split(2).channel = 2*ones(split(2).number,1);
split(2) = nstormapplytransform(split(2), trans);
aligned = nstormcollate(split);
aligned.channel_names = {'a','b'};
clf, nstormshow(aligned,'xy');title('aligned')

%% render raw aligned channels
clf;
[x,y,z,cx,cy,cz] = nstormgrid(aligned,[10 10 10]);
im = nstormrender(aligned,x,y,z,'constant-');
I1 = sqrt(max(im(:,:,:,1),[],3));
I2 = sqrt(max(im(:,:,:,2),[],3));
imshowpair(I1,I2);
RGB = zeros(size(I1,1),size(I1,2),3);
RGB(:,:,1) = 255*(I1-min(I1(:)))/(max(I1(:))-min(I1(:)));
RGB(:,:,2) = 255*(I2-min(I2(:)))/(max(I2(:))-min(I2(:)));
imwrite(uint8(RGB),'aligned.tif');
%% drift correction
K = 10; % number of time blocks
aligned = nstormdrift(aligned,K,100,100);
clf, nstormshow(aligned,'xy');title('drift corrected')
%%
clf;
[x,y,z,cx,cy,cz] = nstormgrid(aligned,[10 10 10]);
im = nstormrender(aligned,x,y,z,'constant-');
I1 = sqrt(max(im(:,:,:,1),[],3));
I2 = sqrt(max(im(:,:,:,2),[],3));
imshowpair(I1,I2);
RGB = zeros(size(I1,1),size(I1,2),3);
RGB(:,:,1) = 255*(I1-min(I1(:)))/(max(I1(:))-min(I1(:)));
RGB(:,:,2) = 255*(I2-min(I2(:)))/(max(I2(:))-min(I2(:)));
imwrite(uint8(RGB),'driftcorrected.tif');
%% decode channels
clf
subplot(121)
assigned = nstormassignchannel(aligned,2,2,100);
subplot(122)
nstormshow(assigned,'xy')
axis ij
saveas(gcf,'assignment.pdf')
%%
clf;
[x,y,z,cx,cy,cz] = nstormgrid(assigned,[10 10 10]);
im = nstormrender(assigned,x,y,z,'sigma');
I1 = max(im(:,:,:,1),[],3);
I2 = max(im(:,:,:,2),[],3);
imshowpair(I1,I2);
RGB = zeros(size(I1,1),size(I1,2),3);
RGB(:,:,1) = 255*(I1-min(I1(:)))/(max(I1(:))-min(I1(:)));
RGB(:,:,2) = 255*(I2-min(I2(:)))/(max(I2(:))-min(I2(:)));
imwrite(uint8(RGB),'assigned.tif');
