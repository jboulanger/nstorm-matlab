function [g,r] = nstormpcf2(molecules, rmax)
% [g,r] = nstormpcf(molecules, rmax)
%
% Pair correlation function between the two channels
%
% Count the average number of point around a shell centered in each point
% and normalize it by the expected number of point in the shell of radius r
% if the point process was random (density x 4 x PI x r^2 x dr).
%
% The code does not correct for edge effects at the moment
%
% Input:
%  molecules: structure with array x,y,z
%  rmax     : maximum radius
% 
% Output:
%  g        : pair correlation function
%  r        : radius vector
%
% Jerome Boulanger
s1 = molecules.channel == 1;
X = [molecules.x(s1), molecules.y(s1), molecules.z(s1)];
s2 = molecules.channel == 2;
Y = [molecules.x(s2), molecules.y(s2), molecules.z(s2)];
shp = alphaShape(X); % compute the volume
v = shp.volume;
n = size(X,1);
rho = n / v; % density
dr = rmax/50;
edges = dr:dr:rmax+dr;
r = dr:dr:rmax;
D = pdist2(X,Y);
g = histcounts(D,edges);
g = 0.5 * g ./ (rho*4*pi*r.^2*dr*(n-1)/2);
if nargout == 0
plot(r,g,r,ones(size(r)));
title('Pair cross correlation')
xlabel('Radius [nm]')
ylabel('g(r)')
axis tight
axis([0, rmax, 0, max(g)]);
end