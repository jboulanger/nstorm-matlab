function storm = nstormread(filename)
%
% storm = NSTORMREAD(filename)
% coordinates = NSTORMREAD()
%
% Read N-STORM List.txt file
%
% If not filename is specified, a dialog pops up to select a file
%
% Example :
%  s = nstormread();
%  figure, plot3(s.x,s.y,s.z,'.'), axis equal
%
% Jerome Boulanger 2017-2020
if nargin < 1
    [name, folder] = uigetfile('*.txt', 'Pick a N-STORM list file');
    filename = [folder filesep name];
end

if 1 % use matlab readtable to load the file 
    t = readtable(filename);
     fields = ...
        {{'x','Xw'},...
        {'y','Yw'},...
        {'z','Zc'},...
        {'width','Width'},...
        {'height','Height'},...
        {'background','BG'},...
        {'ax','Ax'},...
        {'phi','Phi'},...
        {'photons','Photons'},...
        {'area','Area'},...
        {'length','Length'},...
        {'localization_accuracy','LateralLocalizationAccuracy'}};
    storm = struct;
    idx = ~strcmp(t.ChannelName,'Z Rejected');
    for k = 1:numel(fields)
        vals = t.(fields{k}{2});
        storm = setfield(storm, fields{k}{1}, vals(idx));
    end
    [names, ~, ids] = unique(t.ChannelName(idx));
    storm.channel_names = names;
    storm.channel = ids;
    storm.number = numel(storm.x);
    % retreive the photon/count calibration
    storm.photon_per_grayvalue = mean(storm.photons./storm.area);
    % init sigmax,sigmay and sigmaz with localization accuracy
    storm.sigmax = storm.localization_accuracy;
    storm.sigmay = storm.localization_accuracy;
    storm.sigmaz = storm.localization_accuracy;
else

    
    % Open the file in read-only mode.
    fid = fopen(filename,'r');
    % If the file is openned
    if fid > 0
        % Read the file, skiping the header
        C = textscan(fid,'%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f','Delimiter','\t','HeaderLines',1,'Whitespace','');
        % Close the file.
        fclose(fid);
    else
        error('Could not open file %s', filename);
    end
    
    % Create a structure and remove z-rejected molecules
    fields = ...
        {{'x',22},...
        {'y',23},...
        {'z',17},...
        {'width',6},...
        {'height',5},...
        {'ax',9},...
        {'photons',18},...
        {'area',6},...
        {'localization_accuracy',20}};
    storm = struct;
    idx = ones(size(C{1})); %~strcmp(C{1},'Z Rejected');
    for k = 1:numel(fields)
        vals = C{fields{k}{2}};
        storm = setfield(storm, fields{k}{1}, vals(idx));
        
    end
    [names, ~, ids] = unique(C{1}(idx));
    storm.channel_names = names;
    storm.channel = ids;
    storm.number = numel(storm.x);
end
% fields of the _list.txt file:
% 1 X
% 2 Y
% 3 Xc
% 4 Yc
% 5 Height
% 6 Area
% 6 Width
% 8 Phi
% 9 Ax
% 10 BG
% 11 I
% 12 Frame
% 13 Length
% 14 Link
% 15 Valid
% 16 Z
% 17 Zc
% 18 Photons
% 19 Lateral Localization Accuracy
% 20 Xw
% 21 Yw
% 22 Xwc
% 23 Ywc

