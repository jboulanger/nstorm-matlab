function [x,y,z,cx,cy,cz] = nstormgrid(molecules, pixel_size)
% [x,y,z,cx,cy,cz] = nstormgrid(molecules, pixel_size)
% 
% Create a grid for rendering the molecules as an image
% 
% Input:
%  molecules :  structure with position in fields x,y,z and localization
%  precision in sigmax, sigmay, sigmaz
%  pixel_size : array of 3 pixel size in x,y,z
%
% Ouput
%   x,y,z : meshgrid on the domain coverted by the molecules
%   cx,cy,cz : corresponding axis
%
% Jerome Boulanger

x0 = 0.99*min(molecules.x);
y0 = 0.99*min(molecules.y);
z0 = 0.99*min(molecules.z);
x1 = 1.01*max(molecules.x);
y1 = 1.01*max(molecules.y);
z1 = 1.01*max(molecules.z);
cx = x0:pixel_size(1):x1+pixel_size(1);
cy = y0:pixel_size(2):y1+pixel_size(2);
cz = z0:pixel_size(3):z1+pixel_size(3);

[x,y,z] = meshgrid(cx,cy,cz); 
fprintf('nstorm grid [%d x %d x %d] pixels/[%.2f x %.2f x %.2f]um@[%.2f x %.2f x %.2f]nm\n',...
    size(x,1), size(x,2), size(x,3), [x1-x0,y1-y0,z1-z0]/1000, pixel_size);