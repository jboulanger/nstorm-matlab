function storm = nstormformatfields(molecules,fields)
% storm = nstormformatfields(molecules,fields)
%
% Reformat the fields of a structure to make it compatible with the rest of
% the code. 
% 
% The channels are stored are integer and a a list of unique names
%
% Input:
%  molecules : input structures with input fields (.X, .Y, .Z) 
%  fields : a cell of cell acting as a map :
%             fields{1}{1} = 'outputfield1'
%             fields{1}{2} = 'inputfield1'
%  
%  Output:
%   storm : a structure with the output fields defined by the parameter fields
%
%  Compulsory output fields are x, channel, photons, area,
%  localization_accuracy
%
% Jerome Boulanger 2020

storm = struct;
for k = 1:numel(fields)
    vals = molecules.(fields{k}{2});
    storm = setfield(storm, fields{k}{1}, vals);
end
[names, ~, ids] = unique(storm.channel);
storm.channel_names = names;
storm.channel = ids;
storm.number = numel(storm.x);
% retreive the photon/count calibration
storm.photon_per_grayvalue = mean(storm.photons./storm.area);
% init sigmax,sigmay and sigmaz with localization accuracy
storm.sigmax = storm.localization_accuracy;
storm.sigmay = storm.localization_accuracy;
storm.sigmaz = storm.localization_accuracy;