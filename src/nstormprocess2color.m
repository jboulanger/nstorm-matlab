function nstormprocess2color(filename)
nframes = numel(imfinfo(filename));

% compute a background image
bg = compute_background(filename);

f = fspecial('gaussian',[5,5],1) - fspecial('gaussian',[5,5],2);
pfa = 0.1;
pixel_size_um = 0.065;
st = strel('disk',1);
P = struct([]);
tic;
for k = 10:10
    %frame = imread(filename, k);
    frame = sqrt(double(imread(filename, k))) - bg;
    [x,y,height,background] = detect_molecules(frame,f,pfa,st);
    imshow(frame,[]), hold on;plot(x,y,'o'), hold off, drawnow;
    P(k).x = x;
    P(k).y = y;
    P(k).t = k * ones(size(x,1),1);
    P(k).height = height;
    P(k).background = background;
    P(k).number = numel(x);
end
toc;



function bg = compute_background(filename)
bg = sqrt(double(imread(filename,1)));
for k=1:nframes
    bg = bg + sqrt(double(imread(filename,k)));
end
bg = bg / nframes;

