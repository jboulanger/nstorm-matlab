function [R,cx,cy,cz] = nstormfscmap(molecules, pixel_size, roi_size)
% R = nstormmapfsc(molecules, a)
%
% Compute a resolution map using a Fourier shell correlation
%
% Jerome Boulanger 2020

[~,~,~,cx,cy,cz] = nstormgrid(molecules, roi_size);

R = zeros(numel(cy),numel(cx),numel(cz)); 
for kz = 1:numel(cz)-1
    for ky = 1:numel(cy)-1
        for kx = 1:numel(cx)-1
            idx = find(molecules.x >= cx(kx) & molecules.x <= cx(kx+1) & ...
                molecules.y >= cy(ky) & molecules.y <= cy(ky+1) & ...
                molecules.z >= cz(kz) & molecules.z <= cz(kz+1));
            if numel(idx) > 10
                [g,r,r0] = nstormfsc(nstormsubset(molecules,idx), pixel_size);
                R(ky,kx,kz) = 1/r0;
            else
                R(ky,kx,kz) = 0;
            end
        end
    end
end

if nargout == 0
    imagesc(cx/1000,cy/1000,mean(R,3));
    %hold on;
    %plot(molecules.x/1000,molecules.y/1000,'.','color',[0.9 0.9 0.9 0.01]);
    %hold off
    colorbar;
    axis equal; axis tight
    title('Resolution map [nm]')
    xlabel('X [um]');
    ylabel('Y [um]'); 
end