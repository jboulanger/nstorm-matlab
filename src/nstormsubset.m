function crop = nstormsubset(molecules,id)
% crop = nstormsubset(molecules,id)
%
% Extract a subset of molecules coordinates based on an array id
%
% 
fn = fieldnames(molecules);
crop = struct;
for k = 1:numel(fn)
    %fn{k}
    if strcmp(fn{k},'channel_names') || strcmp(fn{k},'number') || strcmp(fn{k},'photon_per_grayvalue') || strcmp(fn{k},'zcalib')
        crop = setfield(crop,fn{k}, getfield(molecules,fn{k}));
    else
        values = getfield(molecules, fn{k});
        crop = setfield(crop, fn{k}, values(id));
    end
end
crop.number = numel(crop.x);