function sigma = nstormthomsonlocalizationaccuracy(molecules, pixel_size)
% sigma = nstormthomsonlocalizationaccuracy(molecules)
%
% Compute the localization accuracy following the equation (17) from
% (Thompson, Biophysisical Journal, 2002)
%
% Jerome Boulanger

s = molecules.width/2;
a = pixel_size;
N = molecules.area * molecules.photon_per_grayvalue;
b = sqrt(molecules.background*molecules.photon_per_grayvalue.*molecules.length);

sigma = sqrt( (s.^2 + a^2/12)./N + (8 * pi * s.^4 .* b.^2) ./ (a^2.*N.^2));


plot(molecules.localization_accuracy, sigma,'.');
hold on
vmax = max(molecules.localization_accuracy);
plot([0,vmax],[0,vmax]);
hold off
xlabel('NSTORM Lateral Localization Accuracy')
ylabel('Thompson et al')
grid on
axis tight
axis equal