function [I,p,G,xy] = nstormrender1(molecules,k,n,pixel_size)
% [I,p,G,xy] = nstormrender1(molecules,k,n,pixel_size)
%
% Render a molecule of index k from the listin a n x n image with a
% given pixel size
%

G = @(p,xy) p(1) + p(2) * exp(- 2 * ( ((xy(:,:,1)-p(3)) / p(5)).^2 + ((xy(:,:,2)-p(4)) / p(6)).^2 ));
xc = round(molecules.x(k) / pixel_size);
yc = round(molecules.y(k) / pixel_size);
ax = pixel_size*((xc - (n-1)/2):(xc + (n-1)/2));
ay = pixel_size*((yc - (n-1)/2):(yc + (n-1)/2));
[x,y] = meshgrid(ax,ay);
xy(:,:,1) = x;
xy(:,:,2) = y;
p(1) = molecules.background(k);
p(2) = molecules.height(k);
p(3) = molecules.x(k);
p(4) = molecules.y(k);
p(5) = molecules.width(k);
p(6) = molecules.width(k)/molecules.ax(k);
I = G(p,xy);
