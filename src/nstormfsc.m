function [f,r,r0] = nstormfsc(molecules, pixel_size, method)
% [f,r,r0] = nstormfsc(molecules, pixel_size)
%
% Fourier Shell correlation
%
% Input:
%  molecules   : structure with fields x,y,z,sigmax,sigmay,sigmaz
%  pixel_size  : pixel size  used for the rendering
%  method      : method for rendering the image
%
% Output:
%  If there is no output, a plot is produced
%  f  : Fourier Shell correlation
%  r  : scale
%  r0 : resolution as f(r0) < 1/7
%
% Jerome Boulanger 2020

if nargin < 3
    method = 'sigma';
end

n = molecules.number;
id = rand(1,n) > 0.5;
[x,y,z,cx,cy,cz]  = nstormgrid(molecules, pixel_size);
set1 = nstormsubset(molecules, id);
F1 = fftn(nstormrender(set1,x,y,z,method));
set2 = nstormsubset(molecules, ~id);
F2 = fftn(nstormrender(set2,x,y,z,method));
kx = kspace(size(F1,2), cx(2)-cx(1));
ky = kspace(size(F1,1), cy(2)-cy(1));
kz = kspace(size(F1,3), cz(2)-cz(1));
[Kx,Ky,Kz] = meshgrid(kx,ky,kz);
a = F1.*conj(F2);
b = abs(F1).^2;
c = abs(F2).^2;
R = sqrt(Kx.^2+Ky.^2+Kz.^2);
r = linspace(0,min([max(kx),max(ky),max(kz)]),30);
for k = 1:numel(r)-1
    I = R >= r(k) & R < r(k+1);
    f(k) = abs(sum(a(I))) ./ sqrt(sum(b(I)) .* sum(c(I)));
end
r = r(1:end-1);

% find the resolution with a linear interpolation
k = find(f>1/7 & circshift(f,-1)<=1/7,1);
if ~isempty(k)
    r0 = r(k) + (r(k+1) - r(k)) * (1/7 - f(k)) / (f(k+1)-f(k));
else
    r0 = max(r);
end

if nargout == 0
    plot(r,f,r,1/7*ones(size(r)),r0,1/7,'ro');
    title(sprintf('Fourier shell correlation\nResolutions:%.2fnm',1/r0))
    xlabel('Spatial frequency [nm^{-1}]');
    ylabel('FSC');
    axis square, grid on
end


function k = kspace(N,s)
k = [0:floor(N/2)-1, floor(-N/2):-1] / (N*s);

