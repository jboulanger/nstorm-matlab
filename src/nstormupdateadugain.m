function molecules = nstormupdateadugain(molecules, photon_per_adu, pixel_size)
% molecules = nstormupdateadugain(molecules, photon_per_adu)
%
% Uupdate the photon per ADU, recompute the photons number using the
% area value and the lateral localization accuracy
% 
% Input:
%   molecules: molecule list
%   photon_per_adu: new value for photon per adu factor
%
% Output:
%  molecules: updated molecule list
%
% Jerome Boulanger 2020

fprintf('Original photon/ADU was %f\n', molecules.photon_per_grayvalue);
molecules.photon_per_grayvalue = photon_per_adu;
molecules.photons = molecules.area * photon_per_adu;
molecules.localization_accuracy = nstormthomsonlocalizationaccuracy(molecules, pixel_size);