function [vol, cnt, dia] = nstormshowcluster(molecules, id)

subplot(211);
scatter3(Xc(:,1),Xc(:,2),Xc(:,3),10,idc(:),'.');

hold on;
vol = zeros(1,Nc);
cnt = zeros(1,Nc);
dia = zeros(1,Nc);
cmap = parula(Nc);
for c = 1:Nc
    shp = alphaShape(X(id==c,1),X(id==c,2),X(id==c,3),2*epsilon);
    vol(c) = shp.volume;
    dia(c) = 2*(3*vol(c)/(4*pi)).^(1/3);
    cnt(c) = numel(find(id==c));
    if vol(c) > 0
        plot(shp,'FaceColor',cmap(c,:),'EdgeAlpha',0,'FaceAlpha',0.7);
    end
   
end
hold off
axis equal
title(sprintf('DBSCAN: %d clutser',Nc))
xlabel('X [nm]');
ylabel('Y [nm]');
zlabel('Z [nm]');
subplot(212);
hist(dia(vol>0));
title(sprintf('Diameter (median:%f)',median(dia(vol>0))));
xlabel('Diameter [nm]')
ylabel('Count')
axis square