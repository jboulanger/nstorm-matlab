function dst = nstormsplit(molecules, idx)

Ks = unique(idx);
for k=1:numel(Ks)
    dst(k) = nstormsubset(molecules, idx==Ks(k));
end
