function nstormshow(storm, mode)
% nstormshow(storm, mode)
%
% Display a set of storm coordinates
%
% input:
%  storm: structure with fields 'x', 'y' 'z' 'channel' and 'channel_names'
%  mode : either 'default', 'xy','xz','yz','scatter3'
%
% jerome boulanger 2017 -2018

if nargin < 2
    mode = 'default';
end

switch lower(mode)
    case 'xy'
        for i = 1:numel(storm.channel_names)
            idx = (storm.channel == i);
            plot(storm.x(idx), storm.y(idx),'.');
            hold on;
        end
        hold off
        axis equal;
        xlabel('X [nm]');
        ylabel('Y [nm]');
    
        grid on; box on; axis tight
        legend(storm.channel_names);
    case 'xz'
        for i = 1:numel(storm.channel_names)
            idx = (storm.channel == i);
            plot(storm.x(idx), storm.z(idx),'.');
            hold on;
        end
        hold off
        axis equal;
        xlabel('X [nm]');
        ylabel('Y [nm]');
        zlabel('Z [nm]');
        grid on; box on;  axis tight
        legend(storm.channel_names);
    case 'yz'
        for i = 1:numel(storm.channel_names)
            idx = (storm.channel == i);
            plot(storm.y(idx), storm.z(idx),'.');
            hold on;
        end
        hold off
        axis equal;
        xlabel('X [nm]');
        ylabel('Y [nm]');
        zlabel('Z [nm]');
        grid on; box on;  axis tight
        legend(storm.channel_names);
      case 'scatter3'        
        scatter3(storm.y, storm.z, storm.localization_accuracy,  storm.channel, 'filled');            
        axis equal;
        xlabel('X [nm]');
        ylabel('Y [nm]');
        zlabel('Z [nm]');
        grid on; box on;  axis tight
        legend(storm.channel_names);  
    otherwise
        for i = 1:numel(storm.channel_names)
            idx = (storm.channel == i);
            plot3(storm.x(idx), storm.y(idx), storm.z(idx),'.');
            hold on;
        end
        hold off
        axis equal;
        xlabel('X [nm]');
        ylabel('Y [nm]');
        zlabel('Z [nm]');
        grid on; box on;
        legend(storm.channel_names);

end