function [id,isnoise,vol,cnt,dia] = nstormdbscan(molecules,epsilon,minpts)
%  [id,isnoise,vol,cnt,dia] = nstormdbscan(molecules,epsilon,minpts)
%
% Cluster analysis using DBSCAN algorithm
%
% Input:
%  molecules : molecules list
%  epsilson  : distance
%  minpts    : min number of poibnts per cluster
%
% Ouput
%  id : identifiers
%
%
% Jerome Boulanger 2020

X = [molecules.x, molecules.y, molecules.z];
[id,isnoise] = dbscan(X,epsilon,minpts);
idc = id(~isnoise);
Xc = X(~isnoise,:);
Nc = numel(unique(idc));

scatter3(molecules.x, molecules.y, molecules.z, 1000, id,'.')
title('DBSCAN clustering')
xlabel('X [um]')
ylabel('Y [um]')
zlabel('Z [um]')