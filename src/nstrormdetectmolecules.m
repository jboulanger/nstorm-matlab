function molecules = nstrormdetectmolecules(frame,f,pfa,st,frame_number,pixel_size)

if ~isa(frame,'double')
    frame = double(frame);
end
b = 30; % border width

% pre filter the image with a difference of gaussian filter
dog = imfilter(sqrt(frame), f);

% create a mask for bright pixels for each half
msk = zeros(size(frame));
msk(b:256-b,b:end-b) = get_mask(dog(b:256-b,b:end-b),pfa,st);
msk(256+b:end-b,b:end-b) = get_mask(dog(256+b:end-b,b:end-b),pfa,st);
%msk(b:end-b,b:end-b) = get_mask(dog(b:end-b,b:end-b),pfa,st);

% look for local maxima
lmax = imdilate(dog, st)==dog;

% create a mask of candidate locations
peak = msk .* lmax > 0;

% find the coordinated of the pixels
[y,x] = find(peak);

% fit all the molecules
r = 3;
[xi,yi] = meshgrid(-r:r,-r:r);
xy(:,:,1) = xi;
xy(:,:,2) = yi;
model = @(p,xy) p(1) + p(2) * exp(- 2 * ( ((xy(:,:,1)-p(3)) / p(5)).^2 + ((xy(:,:,2)-p(4)) / p(6)).^2 ));
height = zeros(numel(x),1);
background = zeros(numel(x),1);
good = zeros(numel(x),1);
sigmax = zeros(numel(x),1);
sigmay = zeros(numel(x),1);
resnorm = zeros(numel(x),1);
for k = 1:numel(x)
    I = frame(y(k)-r:y(k)+r,x(k)-r:x(k)+r)/100;
    pbase = [min(I(:));max(I(:))-min(I(:));0;0;2;2];
    lb = [0.9*min(I(:));min(I(:));-r;-r;0.1;0.1];
    ub = [max(I(:))-min(I(:));max(I(:))-min(I(:));r;r;3*r;3*r];
    opts = optimset('Display','off');
    [phat,rnorm,residual,exitflag,output,lambda,J] = lsqcurvefit(model,pbase,xy,I,[],[],opts);
    J = full(J);
    if cond(J) < 1000
        C = inv(J'*J)*var(residual(:));
    else
        C = 1000*diag(6,6);
    end
    if abs(phat(3)) < 0.5*r &&  abs(phat(4)) < 0.5*r
        x(k) = (x(k) + phat(3)) * pixel_size;
        y(k) = (y(k) + phat(4)) * pixel_size;
    end
    height(k) = phat(2) * 100;
    background(k) = phat(3) * 100;
    good(k) = cond(J) < 1000;
    sigmax(k) = sqrt(C(3,3)) * pixel_size;
    sigmay(k) = sqrt(C(4,4)) * pixel_size;
    resnorm(k) = rnorm;
end

molecules.x = x;
molecules.y = y;
molecules.height = height;
molecules.background = background;
molecules.frame = frame_number*ones(size(x));
molecules.good = resnorm < 2*mean(resnorm);
molecules.sigmax = sigmax;
molecules.sigmay = sigmay;
molecules.resnorm = resnorm;


function msk = get_mask(dog,pfa,st)
thres = mean(dog(:)) + norminv(1-pfa) * std(dog(:),1);
msk = dog > thres;
msk = imopen(msk,st);
