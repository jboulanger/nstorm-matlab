function nstormimshow(im,cx,cy,cz)
% nstormimshow(im,cx,cy,cz)
%
% Display the image as max intensity projection in xy,xz,yz.
%
subplot(221)
imagesc(cx,cy,squeeze(sqrt(max(im,[],3))));
axis equal
axis tight
xlabel('X [nm]')
ylabel('Y [nm]')
subplot(222)
imagesc(cz,cy,squeeze(max(im,[],2)));
axis equal
axis tight
xlabel('Z [nm]')
ylabel('Y [nm]')
subplot(223)
imagesc(cx,cz,squeeze(permute(max(im,[],1),[1 3 2 4])));
axis equal
axis tight
xlabel('X [nm]')
ylabel('Z [nm]')