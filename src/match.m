function [d,p] = match(X1,X2,t1,t2)
% Compute distance and permutation for X1,X2 taking into account frames
% X1(p,:) matches X2(:)
ts = unique(t1);
p = zeros(size(X1,1),1);
d = zeros(size(X1,1),1);
for k = 1:numel(ts)
    id1 = find(t1 == ts(k));
    id2 = find(t2 >= ts(k)-1 & t2 <= ts(k)+1);
    if ~isempty(id2)
        try
        D = pdist2(X1(id1,:),X2(id2,:));
        [dk,pk] = min(D,[],2);
        p(id1) = id2(pk);
        d(id1) = dk;
        catch e
            keyboard
        end
    else
        D = pdist2(X1(id1,:),X2);
        [dk,pk] = min(D,[],2);
        p(id1) = pk;
        d(id1) = dk; 
    end
end
