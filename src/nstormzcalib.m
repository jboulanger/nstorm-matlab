function molecules = nstormzcalib(molecules)
% molecules = nstormzcalib(molecules)
%
% Estimate calibration from existing width, ax and z fields.
% The new molecule list will contain a zcalib field after calibration
%
% The calibration function is from (Huang et al, Science 2008)
%
% Jerome Boulanger

for c = 1:numel(molecules.channel_names)
    [wxy,px,py,z0] = zcalib(nstormsubset(molecules, molecules.channel==c));
    molecules.zcalib(c).wxy = wxy; 
    molecules.zcalib(c).px = px;
    molecules.zcalib(c).py = py;
    molecules.zcalib(c).wx = wxy(px,z0);
    molecules.zcalib(c).wy = wxy(py,z0);
    molecules.zcalib(c).z0 = wxy(py,z0);
end

function [wxy,px,py,z0] = zcalib(molecules)

% Calibration function Wxy = f(z)
wxy = @(p,z) p(1) + sqrt(max(0,1+ ((z-p(2))./p(3)).^2 + p(4) * ((z-p(2))./p(3)).^3+ p(5) * ((z-p(2))./p(3)).^4));

opts = optimset('Display','off');

id = ceil((molecules.number-1) * rand(min(100000,molecules.number),1));
ax = molecules.ax(id);
wx = molecules.width(id);
wy = wx ./ ax;

z = molecules.z(id);
z0 = linspace(min(z),max(z));

a = polyfit(z,wx,2);
px = [a(3),0,20,0.1,0.1];
px = lsqcurvefit(wxy,px,z,wx,[],[],opts);

b = polyfit(z,wy,2);
py = [b(3),-400,20,0.1,0.1];
py = lsqcurvefit(wxy,py,z,wy,[],[],opts);
subplot(221)
plot(z,wx,'.',z,wy,'.',z0,wxy(px,z0),z0,wxy(py,z0));
%plot(z,wx,'.',z,wy,'.',z0,a(1)*z0.^2+a(2)*z0+a(3),z0,b(1)*z0.^2+b(2)*z0+b(3));
axis square
title('Z calibration curves')
legend('Wx','Wy');
grid on
xlabel('z [nm]')
ylabel('width [nm]')


subplot(222)
plot(z, ax,'.', z0, wxy(px,z0)./wxy(py,z0))
grid on
xlabel('z [nm]')
ylabel('Wx/Wy')
axis square

subplot(223)
plot(sqrt(wx), sqrt(wy),'.', sqrt(wxy(px,z0)), sqrt(wxy(py,z0)),'-')
grid on
xlabel('sqrt(Wx)')
ylabel('sqrt(Wy)')
axis square

